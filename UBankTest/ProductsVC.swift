//
//  ProductsVC.swift
//  UBankTest
//
//  Created by Alexey Yakunin on 23/01/2018.
//  Copyright © 2018 Alexey Yakunin. All rights reserved.
//

import UIKit

final class ProductsLogic {
	private let store: [Product : [Transaction]]
	
	private let dataProvider = DataProvider()

	init() {
		var tempStore = [Product : [Transaction]]()
		
		for transaction in dataProvider.transactions {
			if let transactions = tempStore[transaction.sku] {
				tempStore[transaction.sku] = transactions + [transaction]
			} else {
				tempStore[transaction.sku] = [transaction]
			}
		}
		
		store = tempStore
	}

	var products: [Product] {
		return Array(store.keys)
	}

	func transactions(forProduct product: Product) -> [Transaction] {
		guard let transactions = store[product] else {
			return []
		}

		return transactions
	}

	func numberOfTransactions(forProduct product: Product) -> Int {
		return transactions(forProduct: product).count
	}
}

class ProductsVC: UIViewController {

	@IBOutlet weak var tableView: UITableView!

	private let logic = ProductsLogic()

	override func viewDidLoad() {
        super.viewDidLoad()
		
		tableView.delegate = self
		tableView.dataSource = self
    }

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "ShowTransactions") {
			if let transactionsVC = segue.destination as? TransactionsVC, let indexPath = self.tableView.indexPathForSelectedRow {
				transactionsVC.logic = TransactionsLogic(transactions: logic.transactions(forProduct: logic.products[indexPath.row]))
			}
		}
	}
}

extension ProductsVC: UITableViewDataSource, UITableViewDelegate {
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return logic.products.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell =  tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath)

		let product = logic.products[indexPath.row]
		cell.textLabel?.text = product
		cell.detailTextLabel?.text = "\(logic.numberOfTransactions(forProduct: product)) transactions"

		return cell
	}
}
