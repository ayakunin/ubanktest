//
//  TransactionsVC.swift
//  UBankTest
//
//  Created by Alexey Yakunin on 23/01/2018.
//  Copyright © 2018 Alexey Yakunin. All rights reserved.
//

import UIKit

struct TransactionsLogic {
	var transactions: [Transaction]
	private var baseCurrencyValues = [Double?]()
	private let converter = Converter(rates: DataProvider().rates)
	
	init (transactions: [Transaction]) {
		self.transactions = transactions
		baseCurrencyValues = transactions.map {
			converter.convert(from: $0.currency, to: Constans.Currencies.base, amount: $0.amountValue)
		}
	}

	struct Constans {
		struct Currencies {
			static let base: Currency = "GBP"
		}
	}

	var title: String? {
		guard let name = transactions.first?.sku else {
			return nil
		}
		return  "Transactions for \(name)"
	}

	var sumForAllTransactions: Double {
		return baseCurrencyValues.reduce(0.0, { (result: Double, transaction: Double?) -> Double in
			return result +  (transaction ?? 0.0)
		})
	}

	func amountInBaseCurrency(for indexPath: IndexPath) -> Double? {
		return baseCurrencyValues[indexPath.row]
	}
}

class TransactionsVC: UIViewController {

	var logic: TransactionsLogic!
	@IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
		tableView.dataSource = self
		tableView.delegate = self
		title = logic.title
    }
}

extension TransactionsVC: UITableViewDataSource, UITableViewDelegate {
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "RateCell", for: indexPath)
		let transaction = logic.transactions[indexPath.row]
		cell.textLabel?.text = "\(transaction.currency) \(transaction.amount)"
		if let amount = logic.amountInBaseCurrency(for: indexPath) {
			cell.detailTextLabel?.text = "\(TransactionsLogic.Constans.Currencies.base) \(amount)"
		} else {
			cell.detailTextLabel?.text = "Unconvertable"
		}

		return cell
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return logic.transactions.count
	}

	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return "Total: \(TransactionsLogic.Constans.Currencies.base) \(logic.sumForAllTransactions)"
	}
}
