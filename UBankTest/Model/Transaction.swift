//
//  Transaction.swift
//  UBankTest
//
//  Created by Alexey Yakunin on 23/01/2018.
//  Copyright © 2018 Alexey Yakunin. All rights reserved.
//

import Foundation

typealias Currency = String
typealias Product = String

struct Transaction: Codable {
	let currency: Currency
	let amount: String
	let sku: String

	var amountValue: Double {
		return Double(amount)!
	}
}
