
//
//  Converter.swift
//  UBankTest
//
//  Created by Alexey Yakunin on 23/01/2018.
//  Copyright © 2018 Alexey Yakunin. All rights reserved.
//



import Foundation

extension Double {
	func rounded(toPlaces places:Int) -> Double {
		let divisor = pow(10.0, Double(places))
		return (self * divisor).rounded() / divisor
	}
}

struct Converter {
	private let rates: [Rate]
	
	init(rates: [Rate]) {
		self.rates = rates
	}

	func convert(from: Currency, to: Currency, amount: Double) -> Double? {
		guard from != to else {
			return amount
		}

		if let rate = rate(first: from, second: to) {
			return (rate.rateValue * amount).rounded(toPlaces: 2)
		}

		return nil
	}

	func rate(first: Currency, second: Currency) -> Rate? {
		var ratesWithoutDuplicates = [Rate]()

		for rate in rates {
			if ratesWithoutDuplicates.contains(where: {
				$0 == rate
			}) {
				continue
			} else {
				ratesWithoutDuplicates.append(rate)
			}
		}

		let root = buildTreeFor(nodes: [TreeNode(value: second)], rates: ratesWithoutDuplicates)

		guard var searchedNode = root.search(first) else {
			return nil
		}

		var currentRate: Double = 1.0
		while let parent = searchedNode.parent {
			guard let rate = rates.first(where: {$0 == Rate(from: searchedNode.value, to: parent.value, rate: "0")}) else {
				return nil
			}

			if rate.from == searchedNode.value {
				currentRate = currentRate * rate.rateValue
			} else {
				currentRate = (currentRate * (1/rate.rateValue).rounded(toPlaces: 2))
			}

			searchedNode = parent
		}

		return Rate(from: first, to: second, rate: "\(currentRate)")
	}
	
	@discardableResult func buildTreeFor(nodes: [TreeNode<Currency>], rates: [Rate]) -> (TreeNode<Currency>) {
		assert(!nodes.isEmpty)//Не сможем конвертнуть, расходимся
		var updatedRates = rates
		var newNodes = [TreeNode<Currency>]()
		for node in nodes {
			let currencies = updatedRates.flatMap {$0.oppositeTo(curr: node.value)}
			
			let tmpNodes = currencies.map {TreeNode(value: $0)}
			
			tmpNodes.forEach {
				node.addChild($0)
			}

			newNodes += tmpNodes
			
			updatedRates = updatedRates.filter { !$0.contain(curr: node.value) }
		}

		if updatedRates.isEmpty {
			var root = nodes.first!
			while let parent = root.parent {
				root = parent
			}
			return root
		} else {
			return buildTreeFor(nodes: newNodes, rates: updatedRates)
		}
	}
	
}

public class TreeNode<T> where T: Equatable {
	public var value: T
	
	public weak var parent: TreeNode?
	public var children = [TreeNode<T>]()

	public init(value: T) {
		self.value = value
	}
	
	public func addChild(_ node: TreeNode<T>) {
		children.append(node)
		node.parent = self
	}
}

extension TreeNode {
	func search(_ value: T) -> TreeNode? {
		if value == self.value {
			return self
		}
		for child in children {
			if let found = child.search(value) {
				return found
			}
		}
		return nil
	}
}

extension TreeNode: CustomStringConvertible {
	public var description: String {
		var s = "\(value)"
		if !children.isEmpty {
			s += " {" + children.map { $0.description }.joined(separator: ", ") + "}"
		}
		return s
	}
}
