//
//  Rate.swift
//  UBankTest
//
//  Created by Alexey Yakunin on 23/01/2018.
//  Copyright © 2018 Alexey Yakunin. All rights reserved.
//

import Foundation

struct Rate: Codable{
	let from: Currency
	let to: Currency
	let rate: String
	
	var rateValue: Double {
		return Double(rate)!
	}

	func contain(curr: Currency) -> Bool {
		return from == curr || to == curr
	}

	func oppositeTo(curr: Currency) -> Currency? {
		guard [from, to].contains(curr) else {
			return nil
		}
		
		return to == curr ? from : to
	}
}

extension Rate: Equatable {
	static func ==(lhs: Rate, rhs: Rate) -> Bool {
		return (lhs.from == rhs.from && lhs.to == rhs.to) || (lhs.from == rhs.to && lhs.to == rhs.from)
	}
}
