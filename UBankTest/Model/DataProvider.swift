//
//  DataProvider.swift
//  UBankTest
//
//  Created by Alexey Yakunin on 23/01/2018.
//  Copyright © 2018 Alexey Yakunin. All rights reserved.
//

import Foundation

private typealias Transactions = [Transaction]
private typealias Rates = [Rate]

struct DataProvider {
	let transactions: [Transaction]
	let rates: [Rate]

	struct Constants {
		struct FileNames {
			static let transactions = "transactions"
			static let rates = "rates"
		}
	}

	typealias Transactions = [Transaction]

	init() {
		if let url = Bundle.main.url(forResource: Constants.FileNames.transactions, withExtension: "plist") , let data = try? Data(contentsOf: url) {
			let decoder = PropertyListDecoder()
			if let trans = try? decoder.decode(Transactions.self, from: data) {
				transactions = trans
			} else {
				transactions = []
			}
		} else {
			transactions = []
		}

		if let url = Bundle.main.url(forResource: Constants.FileNames.rates, withExtension: "plist") , let data = try? Data(contentsOf: url) {
			let decoder = PropertyListDecoder()
			if let rates = try? decoder.decode(Rates.self, from: data) {
				self.rates = rates
			} else {
				rates = []
			}
		} else {
			rates = []
		}
	}
}
